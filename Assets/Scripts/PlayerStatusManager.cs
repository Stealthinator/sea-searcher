using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerStatusManager : MonoBehaviour
{
    private External ExternalActionsAsset;
    

    public int healthmax = 25;
    public int healthcurrent = 25;
    public int attackDamage = 1;
    public int coinsObtained = 0;
    public int coinsTotal = 0;
    public int treasureObtained = 0;
    public int treasureTotal = 0;
    public bool OnShip = false;
    public bool InShipRange = false;
   
    private bool isMenuOpen = false;
    private bool isMapOpen = false;

    public GameObject FullMap;
    public GameObject FullMapCamera;
    public GameObject ScreenTexts;
    public GameObject MenuTexts;
    
    //Clown moment for bad coding! 
    public Transform ShipRespawn;
    public GameObject Boat;
    public ShipManager shipManager;
    public PlayerInput shipInput;
    public Camera MainCamera;    
    public Camera BoatCamera;
    public Camera BoatCamera2;        

    public GameObject PlayerBody;
    public PlayerManager playerManager;
    public PlayerInput playerInput;

    public TextMeshProUGUI healthText;
    public TextMeshProUGUI coinsText;
    public TextMeshProUGUI treasureText;

    void awake()
    {
        
    }

    // Start is called before the first frame update
    void Start()
    {
        OnShip = false;
        shipManager.isAnchored = true;
        shipInput.actions.Disable();
        BoatCamera.gameObject.SetActive(false);
        BoatCamera2.gameObject.SetActive(false);
        MainCamera.gameObject.SetActive(true);
        playerInput.actions.Enable();

        healthText.text = "Health: " + healthcurrent + "/" + healthmax;
        coinsText.text = "Coins: " + coinsObtained + "/" + coinsTotal;
        treasureText.text = "Treasures: " + treasureObtained + "/" + treasureTotal;
        MenuTexts.SetActive(false);
        FullMap.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

   
    
    void OnSwapModes(InputValue input)
    {
       if(OnShip == true)
        {
             print("exiting ship");
            
            OnShip = false;
            PlayerBody.SetActive(true);
            PlayerBody.transform.position = new Vector3(ShipRespawn.position.x, ShipRespawn.position.y, ShipRespawn.position.z);
            shipManager.isAnchored = true;
            shipInput.actions.Disable();
            playerInput.actions.Enable();
            BoatCamera.gameObject.SetActive(false);
            BoatCamera2.gameObject.SetActive(false);
            MainCamera.gameObject.SetActive(true);
            return;
            
        }
        else if (OnShip == false && InShipRange == true)
        {
            print("entering ship");
            
            OnShip = true;
            PlayerBody.SetActive(false);
            shipManager.isAnchored = true;
            playerInput.actions.Disable();
            shipInput.actions.Enable();
            BoatCamera.gameObject.SetActive(true);
            BoatCamera2.gameObject.SetActive(true);
            MainCamera.gameObject.SetActive(false);
            return;
        }
    }
    
    public void updateHealth(int healthChange)
    {
        healthcurrent += healthChange;
       
        if(healthcurrent <= 0)
        {
            SceneManager.LoadScene("LossScene");
        }
        if(healthcurrent >= healthmax)
        {
            healthcurrent = healthmax;
        }
        healthText.text = "Health: " + healthcurrent + "/" + healthmax;
       
    }
    
    public void updateCoins(int coinChange)
    {
        coinsObtained += coinChange;
        coinsText.text = "Coins: " + coinsObtained + "/" + coinsTotal;
        if(coinsObtained >= coinsTotal)
        {
            SceneManager.LoadScene("VictoryScene");
        }
        
    }

    public void updateTreasure(int treasureChange)
    {
        treasureObtained += treasureChange;
        treasureText.text = "Treasures: " + treasureObtained + "/" + treasureTotal;
        if(treasureObtained >= treasureTotal)
        {
            SceneManager.LoadScene("VictoryScene");
        }
    }

    void OnMenu(InputValue input)
    {
        if(isMenuOpen == false)
        {
            isMenuOpen = true;
            ScreenTexts.SetActive(true);
            MenuTexts.SetActive(false);
            
        }
        else
        {
            isMenuOpen = false;
            ScreenTexts.SetActive(false);
            MenuTexts.SetActive(true);
        }
    }

    void OnMap(InputValue input)
    {
        if(isMapOpen == false)
        {
            isMapOpen = true;
            FullMap.SetActive(true);
            FullMapCamera.SetActive(true);
        }
        else
        {
            isMapOpen = false;
            FullMap.SetActive(false);
            FullMapCamera.SetActive(false);
        }
    }

    public void quitGame()
    {
        Application.Quit();
    }

    public void mainMenu()
    {
        SceneManager.LoadScene("MainMenuScene");
    }
}
