using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicSource : MonoBehaviour
{
    public AudioSource audioSource;

    void Start()
    {
        if(Options.Instance != null)
        {
            audioSource.volume = Options.Instance.VolumeLevel;
        }
        
    }

    public void Update()
    {
       if(Options.Instance != null)
        {
            audioSource.volume = Options.Instance.VolumeLevel;
        }
    }
}
