using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{

    private bool collided;
    public GameObject bulletParticles;
    public PlayerStatusManager playerStatusManager;
    public int bulletDamage = 2;

    void Awake()
    {
        playerStatusManager = GameObject.Find("PlayerStatusManager").GetComponent<PlayerStatusManager>();
    }

    void OnCollisionEnter(Collision co)
    {
        if(co.gameObject.tag == "Player")
        {
            playerStatusManager.updateHealth(-bulletDamage);
            collided = true;
            var impact = Instantiate (bulletParticles, co.contacts[0].point, Quaternion.identity) as GameObject;

            Destroy (impact, 2);
            Destroy(gameObject);
        }
        else if(co.gameObject.tag != "Bullet" && co.gameObject.tag != "Enemy" &&  !collided)
        {
            
            collided = true;
            var impact = Instantiate (bulletParticles, co.contacts[0].point, Quaternion.identity) as GameObject;

            Destroy (impact, 2);
            Destroy(gameObject);
        }
        
    }
}
