using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    private bool collided;
    public GameObject bulletParticles;

    void Start()
    {
        Destroy(gameObject, 5);
    }
    void OnCollisionEnter(Collision co)
    {
        if(co.gameObject.tag != "Bullet" && co.gameObject.tag != "Player" && !collided)
        {
            
            collided = true;
            var impact = Instantiate (bulletParticles, co.contacts[0].point, Quaternion.identity) as GameObject;

            Destroy (impact, 2);
            Destroy(gameObject);
        }
    }
}
