using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    private LightingManager lightManager;
    public PlayerStatusManager playerStatusManager;
    public Transform player;
    public NavMeshAgent agent;

 
    public int damage = 3;
    public float damageCooldown = 5f;
    public float damageRate = 1;
    public bool playerInDamageRange = false;
    public int health = 5;
    public bool dead = false;

    public float timeBetweenRangedAttacks;
    public bool alreadyAttacked;

    public Transform BulletPoint;
    private Vector3 destination;
    public GameObject Bullet;
    public float projectileSpeed = 30;
    public Camera enemyCamera;

    private float oldRange;
    public float sightRange;
    public float attackRange;
    bool playerInSightRange;
    bool playerInAttackRange;
    public Vector3 walkPoint;
    bool walkPointSet;
    public float walkPointRange;
    
    public LayerMask whatIsPlayer;
    public LayerMask whatIsGround;

    void Awake()
    {
        playerStatusManager = GameObject.Find("PlayerStatusManager").GetComponent<PlayerStatusManager>();
        player = GameObject.Find("Player").GetComponent<Transform>();
        agent = GetComponent<NavMeshAgent>();
    
        lightManager = GameObject.Find("Lighting").GetComponent<LightingManager>();
        oldRange = sightRange;
    }


     void OnCollisionEnter(Collision co)
    {
        if(co.gameObject.tag == "Bullet")
        {
            health -= playerStatusManager.attackDamage;
        }
    }

     void OnTriggerEnter(Collider co)
    {
        if(co.gameObject.tag == "Player")
        {
        print("player entered");
        playerInDamageRange = true;
        }
    }

     void OnTriggerExit(Collider co)
    {
        if(co.gameObject.tag == "Player")
        {
        print("player entered");
        playerInDamageRange = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(playerInDamageRange == true && Time.time >= damageCooldown)
        {
            damageCooldown = Time.time + 1/damageRate;
            playerStatusManager.updateHealth(-damage);
        }
        if(health <= 0)
        {
            dead = true;
            Destroy(gameObject);
        }

        playerInSightRange = Physics.CheckSphere(transform.position, sightRange, whatIsPlayer);
        playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, whatIsPlayer);

        if(!playerInAttackRange && !playerInSightRange) Patrolling();
        if(!playerInAttackRange && playerInSightRange) Chasing();
        if(playerInAttackRange && playerInSightRange) Attacking();

        if(lightManager.TimeOfDay > 18 || lightManager.TimeOfDay < 6)
        {
            sightRange = oldRange/2;
        }
        else{
            sightRange = oldRange;
        }
    }

    void Patrolling()
    {
        if(!dead)
        {
            if(!walkPointSet) SearchWalkPoint();
        if(walkPointSet)
        {
            agent.SetDestination(walkPoint);
        }

        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        if(distanceToWalkPoint.magnitude < 1f)
        {
            walkPointSet = false;
        }
        }
        
    }

    void SearchWalkPoint()
    {
        float randomZ = Random.Range(-walkPointRange, walkPointRange);
        float randomX = Random.Range(-walkPointRange, walkPointRange);
        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

        if(Physics.Raycast(walkPoint, -transform.up, 2f, whatIsGround ))
            walkPointSet = true;
    }

    void Chasing()
    {
        if(!dead)
        {
        agent.SetDestination(player.position);
        }
    }

    void Attacking()
    {
        if(!dead)
        {
        agent.SetDestination(transform.position);
        transform.LookAt(player);

        if(!alreadyAttacked)
        {
            Ray ray = enemyCamera.ViewportPointToRay(new Vector3(0.5f,0.5f, 0));
            RaycastHit hit;

            if(Physics.Raycast(ray, out hit))
            {
                destination = hit.point;
            }
            else
            {
                destination = ray.GetPoint(150);
            }
            InstantiateProjectile();
            
            alreadyAttacked = true;
            Invoke(nameof(ResetAttacking), timeBetweenRangedAttacks);
        }
        }
    }

    void InstantiateProjectile()
    {
        var bulletObject = Instantiate (Bullet, BulletPoint.position, Quaternion.identity) as GameObject;
        bulletObject.GetComponent<Rigidbody>().velocity = (destination - BulletPoint.position).normalized * projectileSpeed;
    }

    void ResetAttacking()
    {
        alreadyAttacked = false;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, sightRange);
    }
}
