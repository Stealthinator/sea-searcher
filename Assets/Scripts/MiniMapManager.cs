using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMapManager : MonoBehaviour
{
    public GameObject player;
    public GameObject boat;

    //Being really lazy and just gonna hope every scene has boat and player for when minimap is enabled.
    void LateUpdate()
    {
        Vector3 newPos;
        
        if(player.activeSelf == true)
        {
            newPos = player.transform.position;
            newPos.y = transform.position.y;
            transform.position = newPos;

        }
        else
        {
            newPos = boat.transform.position;
            newPos.y = transform.position.y;
            transform.position = newPos;
        }
    }
}
