using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clouds : MonoBehaviour
{
    public Transform orbitPoint; 
    public float xSpread;
    public float ySpread;
    public float zSpread;
    public float rotationSpeed;
    public bool rotateClockwise;

    private float timer = 0;

    void Start()
    {
        if(orbitPoint == null)
        {
            orbitPoint = this.transform;
        }
    }
     void rotate()
     {
         if(rotateClockwise)
         {
            float x = -Mathf.Cos(timer) * xSpread;
            float z = Mathf.Sin(timer) * zSpread;
            Vector3 pos = new Vector3(x, ySpread, z);
            transform.position = pos + orbitPoint.position;
         }
         else
         {
            float x = Mathf.Cos(timer) * xSpread;
            float z = Mathf.Sin(timer) * zSpread;
            Vector3 pos = new Vector3(x, ySpread, z);
            transform.position = pos + orbitPoint.position;
         }
     }

    void FixedUpdate()
    {
        timer += Time.deltaTime * rotationSpeed;
        rotate();
        transform.LookAt(orbitPoint);

    }
}
