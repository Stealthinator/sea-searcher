using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject MainMenuHolder;
    public GameObject OptionsHolder;

    public void PlayGame()
    {
        SceneManager.LoadScene("HubIslandScene");
    }

    public void Options()
    {
        MainMenuHolder.SetActive(false);
        OptionsHolder.SetActive(true);
    }

    public void Back()
    {
        MainMenuHolder.SetActive(true);
        OptionsHolder.SetActive(false);
    }
    public void Quit()
    {
        Application.Quit();
    }
}
