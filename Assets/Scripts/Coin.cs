using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{

    public float speed = 1f;
    public int value = 1;
    public PlayerStatusManager playerStatusManager;


    void Awake()
    {
        playerStatusManager = GameObject.Find("PlayerStatusManager").GetComponent<PlayerStatusManager>();
    }
    void FixedUpdate()
    {
        this.transform.Rotate(new Vector3(0,speed, 0)); 
        
    }

    void OnTriggerEnter(Collider colObject)
    {
        print(colObject);
        if(colObject.tag == "Player")
        {
           Destroy(gameObject);
           playerStatusManager.updateCoins(value);
        }
        
    }
}
