using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Options : MonoBehaviour 
{
    public static Options Instance;
    public float VolumeLevel = 1;
    
    void Awake ()   
       {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy (gameObject);
        }
      }

    public void changeGlobalVolume(float newVol)
    {
        VolumeLevel = newVol;
    }
}