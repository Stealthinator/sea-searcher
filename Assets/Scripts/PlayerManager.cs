using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerManager : MonoBehaviour
{

    private ThirdPersonActionsAsset playerActionsAsset;
    private InputAction move;
    private Rigidbody rigidBody;
    private Vector3 movementInput;
    public  GameObject GliderMesh;
    public bool canGlide = true;
    public float glideTime = 3f;
    
    public float movementForce = 1f;
    
    public float jumpForce = 5f;
    public Transform BulletPoint;
    private Vector3 destination;
    public GameObject Bullet;
    public float projectileSpeed = 30;
    public float timeToFire = 2;
    private bool shooting = false;
    public float fireRate = 2;
    

    public float maxSpeed = 5f;
    private Vector3 forceDirection = Vector3.zero;
    [SerializeField]
    private Floater floater;
    [SerializeField]
    private Camera playerCamera;
    [SerializeField]
    private Camera firstpersonplayerCamera;
    void Awake()
    {
        rigidBody = GetComponent<Rigidbody>();
        playerActionsAsset = new ThirdPersonActionsAsset();

    }
    
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if(shooting == true & Time.time >= timeToFire)
        {
            timeToFire = Time.time + 1/fireRate;
            attack();
        }
    }

    private void OnEnable()
    {
        playerActionsAsset.Player.Jump.started += DoJump;
        move = playerActionsAsset.Player.Move;
        playerActionsAsset.Player.Enable();
        rigidBody.useGravity = true;

    }

    private void OnDisable()
    {
        playerActionsAsset.Player.Jump.started -= DoJump;
        playerActionsAsset.Player.Disable();
    }

    private void DoJump(InputAction.CallbackContext obj)
    {
        rigidBody.useGravity = true;
        if(isGrounded() == true)
        {
            forceDirection += Vector3.up * jumpForce;
        }
        else
        {
            return;
        }

    }

    private void FixedUpdate()
    {
        
        forceDirection += move.ReadValue<Vector2>().x * GetCameraRight(playerCamera) * movementForce;
        forceDirection += move.ReadValue<Vector2>().y * GetCameraForward(playerCamera) * movementForce;
        rigidBody.AddForce(forceDirection,ForceMode.Impulse);
        forceDirection = Vector3.zero;
        
        if(rigidBody.velocity.y <= 0f)
        {
            rigidBody.velocity -= 3 * Vector3.down * Physics.gravity.y * Time.fixedDeltaTime;
            if(isGrounded() == true)
            {
            GliderMesh.SetActive(false);
            canGlide = true;
            }
        }
        
        Vector3 horizontalVelocity = rigidBody.velocity;
        horizontalVelocity.y = 0;
        if(horizontalVelocity.sqrMagnitude > maxSpeed * maxSpeed)
        {
            rigidBody.velocity = horizontalVelocity.normalized * maxSpeed + Vector3.up * rigidBody.velocity.y;
        }
        LookAt();
        if(transform.position.y > WaveManager.instance.GetWaveHeight(transform.position.x) || isGrounded() == true)
        {
            floater.enabled = false;
        }
        else
        {
            floater.enabled = true;
        }
    }

    private void LookAt()
    {
        Vector3 direction = rigidBody.velocity;
        direction.y = 0f;
        if(move.ReadValue<Vector2>().sqrMagnitude > 0.1f && direction.sqrMagnitude > 0.1f)
        {
            this.rigidBody.rotation = Quaternion.LookRotation(direction, Vector3.up);
        }
        else
        {
            rigidBody.angularVelocity = Vector3.zero;
        }
    }
    private Vector3 GetCameraForward(Camera playerCamera)
    {
        Vector3 forward = playerCamera.transform.forward;
        forward.y = 0;
        return forward.normalized;
    }

    private Vector3 GetCameraRight(Camera playerCamera)
    {
        Vector3 right = playerCamera.transform.right;
        right.y = 0;
        return right.normalized;
    }

    private bool isGrounded()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, new Vector3(transform.position.x, -1000, transform.position.z));

        if(Physics.Raycast(ray, out hit, 0.5f))
        {
            if(hit.collider != null)
            {
                return true;
            }
        }
        return false;
    }

    private void OnGlide(InputValue input)
    {
        if(isGrounded() == false && canGlide == true)
        {
            GliderMesh.SetActive(true);
            canGlide = false;
            rigidBody.useGravity = false;
            rigidBody.velocity += new Vector3(0,4 - rigidBody.velocity.y,0);
            print("Gliding Start");
            StartCoroutine(glideTimer(glideTime));
            
        }
        
    }
    
    IEnumerator glideTimer(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        rigidBody.useGravity = true;
        print("Gliding End");
    }

     private void OnAttack(InputValue input)
    {
        print("shooting");
        if(shooting == true)
        {
            shooting = false;
        }
        else
        {
            shooting = true;
        }
    }
    private void attack()
    {
        Ray ray = firstpersonplayerCamera.ViewportPointToRay(new Vector3(0.5f,0.5f, 0));
        RaycastHit hit;

        if(Physics.Raycast(ray, out hit))
        {
            destination = hit.point;
        }
        else
        {
            destination = ray.GetPoint(150);
        }
        InstantiateProjectile();
    }

    void InstantiateProjectile()
    {
        var bulletObject = Instantiate (Bullet, BulletPoint.position, Quaternion.identity) as GameObject;
        bulletObject.GetComponent<Rigidbody>().velocity = (destination - BulletPoint.position).normalized * projectileSpeed;
    }


    
}
