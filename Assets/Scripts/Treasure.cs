using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Treasure : MonoBehaviour
{

    public float speed = 1f;
    public int value = 1;

    public float jumpBonus = 0.25f;
    public float speedBonus = 0.25f;
    public float movementBonus = 0.25f;
    public float glideBonus = 0.25f;
    public PlayerStatusManager playerStatusManager;
    public PlayerManager playerManager;

    void Awake()
    {
        playerStatusManager = GameObject.Find("PlayerStatusManager").GetComponent<PlayerStatusManager>();
        playerManager = GameObject.Find("Player").GetComponent<PlayerManager>();
    }
    void FixedUpdate()
    {
        this.transform.Rotate(new Vector3(0,speed, 0)); 
        
    }

    void OnTriggerEnter(Collider colObject)
    {
        print(colObject);
        if(colObject.tag == "Player")
        {
           Destroy(gameObject);
           playerStatusManager.updateTreasure(value);
           playerManager.jumpForce += jumpBonus;
           playerManager.maxSpeed += speedBonus;
           playerManager.movementForce += movementBonus;
           playerManager.glideTime += glideBonus;
        }
        
    }
}
