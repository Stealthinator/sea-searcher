using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndScreenManager : MonoBehaviour
{

    public void OnPlayAgain()
    {
        SceneManager.LoadScene("MainMenuScene");
    }
    
    public void OnQuit()
    {
        Application.Quit();
    }
}
