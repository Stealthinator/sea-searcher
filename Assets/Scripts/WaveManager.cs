using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : MonoBehaviour
{
public static WaveManager instance;

public Renderer waterShader;

public bool daynightWaves = true;
public LightingManager lightingManager;
public float Length = 1;
public float Amplitude = 1;
public float Speed = 1;
public float Offset = 0;
private void Awake()
{
    if (instance == null)
    {
        instance = this;
    }
    else if(instance != this)
    {
        Debug.Log("Instance already exists, destroying object!");
        Destroy(this);
    }
    if(lightingManager == null)
    {
        print("no lighting manager found");
        daynightWaves = false;
    }
}

private void Update()
{

    /* Fix later
    if(daynightWaves == true)
    {
        //waterShader.material.SetFloat("Amplitude",.25f * Mathf.Cos(.5f * lightingManager.TimeOfDay) + 1 );
        //waterShader.material.SetFloat("Length",Mathf.Cos(.5f * lightingManager.TimeOfDay) + 2 );
        //waterShader.material.SetFloat("Speed",.2f * Mathf.Cos(.5f * lightingManager.TimeOfDay) + 1 );
        Amplitude = waterShader.material.GetFloat("_amplitude");
        Length = waterShader.material.GetFloat("_length");
        Speed = waterShader.material.GetFloat("_speed");
        Offset = waterShader.material.GetFloat("_offset");
        
    }
    */
}

public float GetWaveHeight(float _x)
{
    return Amplitude * Mathf.Sin(_x / Length + Offset);
}
}
