using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ShipManager : MonoBehaviour
{
    private ShipActionsAsset shipActionsAsset;
    private ThirdPersonActionsAsset playerActionsAsset;
    private InputAction move;
    private Rigidbody rigidBody;
    private Vector3 movementInput;
    public PlayerStatusManager playerStatusManager;

    public bool isAnchored = false;

    public GameObject AnchorObject;

    [SerializeField]
    private float movementForce = 1f;
    [SerializeField]
    private float jumpForce = 5f;
    [SerializeField]
    private float maxSpeed = 5f;
    private Vector3 forceDirection = Vector3.zero;

    [SerializeField]
    private Camera playerCamera;

    void Awake()
    {
        rigidBody = GetComponent<Rigidbody>();
        shipActionsAsset = new ShipActionsAsset();

    }
    
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnEnable()
    {

        move = shipActionsAsset.Player.Move;
        shipActionsAsset.Player.Enable();

    }

    private void OnDisable()
    {

        shipActionsAsset.Player.Disable();
    }

    private void DoJump(InputAction.CallbackContext obj)
    {
        if(isGrounded() == true)
        {
            forceDirection += Vector3.up * jumpForce;
        }
        else
        {
            return;
        }

    }

    private void FixedUpdate()
    {
        
        forceDirection += move.ReadValue<Vector2>().x * GetCameraRight(playerCamera) * movementForce;
        forceDirection += move.ReadValue<Vector2>().y * GetCameraForward(playerCamera) * movementForce;
        rigidBody.AddForce(forceDirection,ForceMode.Impulse);
        forceDirection = Vector3.zero;
        
        if(rigidBody.velocity.y < 0f)
        {
            rigidBody.velocity -= 3 * Vector3.down * Physics.gravity.y * Time.fixedDeltaTime;
        }
        

        Vector3 horizontalVelocity = rigidBody.velocity;
        horizontalVelocity.y = 0;
        if(horizontalVelocity.sqrMagnitude > maxSpeed * maxSpeed)
        {
            rigidBody.velocity = horizontalVelocity.normalized * maxSpeed + Vector3.up * rigidBody.velocity.y;
        }
        LookAt();
        Anchor();
    }

    private void LookAt()
    {
        Vector3 direction = rigidBody.velocity;
        direction.y = 0f;
        if(move.ReadValue<Vector2>().sqrMagnitude > 0.1f && direction.sqrMagnitude > 0.1f)
        {
            this.rigidBody.rotation = Quaternion.LookRotation(direction, Vector3.up);
        }
        else
        {
            rigidBody.angularVelocity = Vector3.zero;
        }
    }
    private Vector3 GetCameraForward(Camera playerCamera)
    {
        Vector3 forward = playerCamera.transform.forward;
        forward.y = 0;
        return forward.normalized;
    }

    private Vector3 GetCameraRight(Camera playerCamera)
    {
        Vector3 right = playerCamera.transform.right;
        right.y = 0;
        return right.normalized;
    }

    private bool isGrounded()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, new Vector3(transform.position.x, -1000, transform.position.z));

        if(Physics.Raycast(ray, out hit, 0.5f))
        {
            if(hit.collider != null)
            {
                return true;
            }
        }
        return false;
    }

    void OnTriggerEnter(Collider colObject)
    {
        //print(colObject);
        if(colObject.gameObject.CompareTag("Player") == true)
        {
            playerStatusManager.InShipRange = true;
        }
    }

    void OnTriggerExit(Collider colObject)
    {
        //print(colObject);
        if(colObject.gameObject.CompareTag("Player") == true)
        {
          playerStatusManager.InShipRange = false;
        }
        
    }

    private void OnAnchor(InputValue input)
    {
        if(isAnchored == false)
        {
            isAnchored = true;
        }
        else
        {
            isAnchored = false;
        }
    }
    
    private void Anchor()
    {
        if(isAnchored == true)
        {
            AnchorObject.SetActive(true);
            rigidBody.constraints = RigidbodyConstraints.FreezePositionX  | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationZ ;
        }
        else
        {
            AnchorObject.SetActive(false);
            rigidBody.constraints = RigidbodyConstraints.None;
            rigidBody.constraints =  RigidbodyConstraints.FreezeRotationZ ;

      }
    }
    
}
