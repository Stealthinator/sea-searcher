using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointMovement : MonoBehaviour
{


    int CurrentWaypointIndex = 0;
    public Transform[] waypoints;
    public float speed = 1;
    public float WPRadius = 1f;

    void Update()
    {
        if(Vector3.Distance(waypoints[CurrentWaypointIndex].transform.position, transform.position) < WPRadius)
        {
            CurrentWaypointIndex++;
            if(CurrentWaypointIndex >= waypoints.Length)
            {
                CurrentWaypointIndex = 0;
            }
        }
        transform.position = Vector3.MoveTowards(transform.position, waypoints[CurrentWaypointIndex].transform.position, Time.deltaTime * speed);
    }
}
